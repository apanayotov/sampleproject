package com.axway.academy.build;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import com.axway.academy.depndentbuild.SampleClass;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger sLogger = LogManager.getLogger(App.class);
	
	public static void main( String[] args )
    {
		sLogger.error("kor");
        System.out.println( "Hello World!" );
//        SampleClass sample = new SampleClass();
//        sample.sampleMethod();
        sLogger.error("Sample done");
    }
}
